<?php
/*
Template Name: Home
*/
?>

<!-- Header -->
<?php BsWp::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
    <!-- CONTENT -->
    <div class="container-fluid">
    <div class="row post-wrapper intro">
      <div class="col-md-12 post-content">
        <h1>Osynlighetsprojektet</h1>
        <h4 class="description"><?php the_field('description', get_option('page_for_posts')); ?></h4>
      </div>
    </div>


  <?php $i=1; ?>

     <?php while ( have_posts() ) : the_post(); ?>

          <div class="row post-wrapper" id="page<?php echo $i++; ?>">

              <div class="col-md-12 post-content">
                  <?php if ( has_post_thumbnail() ): ?>
                    <div class="col-md-8">
                      <h1 class="pagetitle"><?php the_title(); ?></h1>
                      <?php the_content(); ?>
                    </div>
                    <div class="col-md-4 img-thumb-container">
                      <?php the_post_thumbnail('', array('class' => 'img-thumb')); ?>
                    </div>
                  <?php else : ?>
                    <div class="col-md-12">
                      <h1 class="pagetitle"><?php the_title(); ?></h1>
                      <?php the_content(); ?>
                    </div>
                  <?php endif; ?>

              </div>

          </div>

     <?php endwhile; ?>

<?php wp_reset_query(); ?>
<div id="pagecontact" class="row post-wrapper">
  <div class="col-md-12 post-content">
    <h1>Kontakta Oss</h1>
  </div>
</div>
    </div>
<script type="text/javascript">
  var $ = jQuery.noConflict();
  // This way you can use the '$' as jquery
  console.log('Check home.php to know how to write jQuey on php templates', $('body'));
</script>

<?php BsWp::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
