<nav id="nav_principal">
  <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed navbar-btn menu-btn" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
            <span>Menu</span>
          </button>
          <!-- <button data-toggle="modal" data-target="#menu" type="button" class="hidden-lg hidden-md btn btn-default navbar-btn menu-btn pull-right">Menu</button> -->
          <!-- <a class="navbar-brand" href="<?php //echo home_url(); ?>">
            <img src="<?php //echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" />
    			</a> -->
        </div>
    	<div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
        <button class="hidden-lg hidden-md hidden-sm btn-close-menu" data-toggle="collapse" data-target="#bs-navbar-collapse-1">&times;</button>
    		 <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => false,
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
            ?>
    	</div>
    </div>
</nav>
