<footer class="footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <p>TILLSAMMANS MED</p>
      </div>
      <div class="col-md-8">
        <p>ETT INITIATIV FRÅN</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2"><img class="partner" src="<?php the_field('partner1', get_option('page_for_posts')); ?>"/></div>
      <div class="col-md-2"><br /></div>
      <div class="col-md-2"><img class="partner" src="<?php the_field('partner3', get_option('page_for_posts')); ?>"/></div>
      <div class="col-md-2"><img class="partner" src="<?php the_field('partner4', get_option('page_for_posts')); ?>"/></div>
      <div class="col-md-2"><img class="partner" src="<?php the_field('partner5', get_option('page_for_posts')); ?>"/></div>
      <div class="col-md-2"><img class="partner" src="<?php the_field('partner6', get_option('page_for_posts')); ?>"/></div>
    </div>
  </div>
</footer>
