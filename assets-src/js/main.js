import App from 'app';
import bootstrap from 'bootstrap';
import skrollr from 'skrollr';

window.BREWSER = brewser.BREWSER;
window.FastClick = FastClick;

$(document).on('ready', function init() {
  // Init touch devices
  if (!BREWSER.device.touch) {
    $('html').addClass('do-hovers');
  } else {
    FastClick.attach(document.body);
  }

  var app = new App();
//smooth scroll
  $(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
});
