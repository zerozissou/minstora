class App {
  constructor() {
    this.eventDispatcher = {};
    this.Events = {
      'RESIZE': 'resize',
      'SELECT_NEXT': 'selectNext',
      'SELECT_PREV': 'selectPrev',
      'SELECT': 'select',
      'MAIN_MENU_OPEN': 'mainMenuOpen',
      'GLOBAL_MENU_OPEN': 'globalMenuOpen',
      'MAIN_MENU_OVERLAY_CLICK': 'mainMenuOverlayClick',
      'GLOBAL_MENU_OVERLAY_CLICK': 'globalMenuOverlayClick',
      'THUMBNAILS_ADDED': 'thumbnailsAdded'
    };
    window.sbc = this;
  }
}

module.exports = App;
